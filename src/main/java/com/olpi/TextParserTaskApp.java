package com.olpi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.olpi.domain.*;
import com.olpi.parser.*;
import com.olpi.service.TextService;

public class TextParserTaskApp {

    private static final Logger logger = LogManager
            .getLogger(TextParserTaskApp.class);
    private static final String DIVIDING_LINE = "---------------------\n";

    public static void main(String[] args) {

        File file = new File("./src/main/recources/input.txt");

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

            StringBuilder builder = new StringBuilder();

            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
                builder.append("\n");
            }
            logger.debug("File has been read.");

            Parser sentenceParser = new SentenceParser(null);
            Parser pharagraphParser = new ParagraphParser(sentenceParser);
            Parser textParser = new TextParser(pharagraphParser);

            Text text = (Text) textParser.parse(builder.toString());
            logger.debug("The text has been presented as an object.");
            System.out.print(
                    DIVIDING_LINE + "Initial text:\n\n" + text.toString());

            TextService compositeElementService = new TextService();

            System.out.print(DIVIDING_LINE
                    + "Paragraphs have been sorted by the amount of sentences:\n\n"
                    + compositeElementService
                            .sortParagraphsBySentencesAmount(text));
            System.out.print(DIVIDING_LINE
                    + "Sentences in paragraphs have been sorted by amount of words:\n\n"
                    + compositeElementService
                            .sortSentencesInParagraphsByWordsAmount(text));
            System.out.print(DIVIDING_LINE
                    + "Words in sentences have been sorted by length:\n\n"
                    + compositeElementService
                            .sortWordsInSentencesByLength(text));
            System.out.print(
                    DIVIDING_LINE + "Original text:\n" + text.toString());

        } catch (FileNotFoundException ex) {
            logger.log(Level.ERROR, "File not found!", ex);
        } catch (IOException ex) {
            logger.log(Level.ERROR, "IOException!", ex);
        }
    }
}
