package com.olpi.domain;

public class Word implements TextComponent {

    private String content;

    public Word(String content) {
        this.content = content;
    }

    public int contentSize() {
        return content.length();
    }

    @Override
    public String toString() {
        return content;
    }

}
