package com.olpi.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.olpi.domain.CompositeElement;
import com.olpi.domain.Paragraph;
import com.olpi.domain.Sentence;
import com.olpi.domain.Symbol;
import com.olpi.domain.Text;
import com.olpi.domain.TextComponent;
import com.olpi.domain.Word;

public class TextService {

    private static final Logger logger = LogManager
            .getLogger(TextService.class);

    public String sortParagraphsBySentencesAmount(Text text) {
        logger.debug("Paragraphs are sorted by amount of sentences.");
        return sortComponentsBySize(text);
    }

    public String sortWordsInSentencesByLength(Text text) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < text.getComponents().size(); i++) {
            if (text.getComponents().get(i) instanceof Paragraph) {
                Paragraph paragraph = (Paragraph) text.getComponents().get(i);

                for (int j = 0; j < paragraph.getComponents().size(); j++) {
                    if (paragraph.getComponents().get(j) instanceof Sentence) {
                        Sentence sentence = (Sentence) paragraph.getComponents()
                                .get(j);
                        builder.append(sortComponentsBySize(sentence));
                        builder.append("\n");
                    }
                }
                builder.append("\n");
            }
        }
        logger.debug("Words in each sentences are sorted by length.");
        return builder.toString();
    }

    public String sortSentencesInParagraphsByWordsAmount(Text text) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < text.getComponents().size(); i++) {
            if (text.getComponents().get(i) instanceof Paragraph) {
                Paragraph paragraph = (Paragraph) text.getComponents().get(i);
                builder.append("  ");
                builder.append(sortComponentsBySize(paragraph));
                builder.append("\n");
            }
        }
        logger.debug(
                "Sentences in each paragraphs are sorted by amount of words.");
        return builder.toString();
    }

    private String sortComponentsBySize(CompositeElement element) {
        List<TextComponent> components = new ArrayList<>(
                element.getComponents());
        components.sort((c1, c2) -> c2.contentSize() - c1.contentSize());

        StringBuilder builder = new StringBuilder();

        components.forEach(component -> {
            if (!(component instanceof Symbol)) {
                builder.append(component.toString());
                if (component instanceof Word) {
                    builder.append(" ");
                }
            }
        });
        return builder.toString();
    }

}
