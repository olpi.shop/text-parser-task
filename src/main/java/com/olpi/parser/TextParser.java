package com.olpi.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.olpi.domain.Text;
import com.olpi.domain.CompositeElement;

public class TextParser extends Parser {
    private static final Logger logger = LogManager.getLogger(TextParser.class);

    public TextParser(Parser nextParser) {
        super(nextParser);
    }

    public CompositeElement parse(String input) {
        Text text = new Text();
        logger.debug("Text has been created.");
        String paragraphRegex = "(?sm)^.*?\\.$";
        Pattern pattern = Pattern.compile(paragraphRegex);
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            CompositeElement paragraph = this.getNextParser()
                    .parse(matcher.group());
            text.addComponent(paragraph);
            logger.debug("A paragraph is added into the text.");
        }
        return text;
    }

}
