package com.olpi.parser;

import com.olpi.domain.CompositeElement;

public abstract class Parser {
    private Parser nextParser;

    public Parser(Parser nextParser) {
        this.nextParser = nextParser;
    }

    public Parser getNextParser() {
        return nextParser;
    }

    public abstract CompositeElement parse(String input);

}
