package com.olpi.domain;

public interface TextComponent {

    public String toString();

    public int contentSize();

}
