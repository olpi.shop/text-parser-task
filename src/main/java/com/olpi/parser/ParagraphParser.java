package com.olpi.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.olpi.domain.Paragraph;
import com.olpi.domain.Symbol;
import com.olpi.domain.CompositeElement;

public class ParagraphParser extends Parser {

    private static final Logger logger = LogManager
            .getLogger(ParagraphParser.class);

    public ParagraphParser(Parser nextParser) {
        super(nextParser);
    }

    public CompositeElement parse(String input) {
        CompositeElement paragraph = new Paragraph();
        logger.debug("Paragraph has been created.");
        paragraph.addComponent(new Symbol(' '));
        paragraph.addComponent(new Symbol(' '));

        String senteceRegex = "([A-Z][\\w|\\s|,|\\-|'|\\n]+[\\.])";
        Pattern pattern = Pattern.compile(senteceRegex);
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            CompositeElement sentence = this.getNextParser()
                    .parse(matcher.group());
            paragraph.addComponent(sentence);
            logger.debug("A sentence is added into the paragraph.");
        }
        paragraph.addComponent(new Symbol('\n'));
        return paragraph;
    }

}
