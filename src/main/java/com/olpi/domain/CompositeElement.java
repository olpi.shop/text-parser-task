package com.olpi.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class CompositeElement implements TextComponent {

    private List<TextComponent> components = new ArrayList<>();

    public List<TextComponent> getComponents() {
        return Collections.unmodifiableList(components);
    }

    public void addComponent(TextComponent component) {
        components.add(component);
    }

    public void removeComponent(TextComponent component) {
        components.remove(component);
    }

    public int contentSize() {
        return components.size();
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (TextComponent component : components) {
            builder.append(component.toString());
        }
        return builder.toString();
    }

}
