package com.olpi.parser;

import com.olpi.domain.Sentence;
import com.olpi.domain.Symbol;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.olpi.domain.CompositeElement;
import com.olpi.domain.Word;

public class SentenceParser extends Parser {

    private static final Logger logger = LogManager
            .getLogger(SentenceParser.class);

    public SentenceParser(Parser nextParser) {
        super(nextParser);
    }

    public CompositeElement parse(String input) {
        Sentence sentence = new Sentence();
        logger.debug("Sentence has been created.");
        String[] words = input.split("\\s");

        for (String word : words) {
            if (word.equals("")) {
                sentence.addComponent(new Symbol('\n'));
                logger.debug("Line break is added");
            } else {
                String wordRegex = "(\\w+(-)?\\w*)";
                Pattern pattern = Pattern.compile(wordRegex);
                Matcher matcher = pattern.matcher(word);
                matcher.find();

                if (matcher.start() != 0) {
                    char[] symbols = word.substring(0, matcher.start())
                            .toCharArray();
                    addSymbols(sentence, symbols);
                }
                sentence.addComponent(new Word(matcher.group()));
                logger.debug("New word is added in the sentence.");

                if (matcher.end() != word.length()) {
                    char[] symbols = word
                            .substring(matcher.end(), word.length())
                            .toCharArray();
                    addSymbols(sentence, symbols);
                }
                sentence.addComponent(new Symbol(' '));
            }
        }
        return sentence;
    }

    private void addSymbols(Sentence sentence, char[] symbols) {
        for (char symbol : symbols) {
            sentence.addComponent(new Symbol(symbol));
        }
    }

}
