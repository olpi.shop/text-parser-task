package com.olpi.domain;

public class Symbol implements TextComponent {

    private char content;

    public Symbol(char content) {
        this.content = content;
    }

    public int contentSize() {
        return 1;
    }

    @Override
    public String toString() {
        return String.valueOf(content);
    }

}
